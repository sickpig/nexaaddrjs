/**
 * https://github.com/ealmansi/cashaddrjs
 * Copyright (c) 2017-2020 Emilio Almansi
 * Distributed under the MIT software license, see the accompanying
 * file LICENSE or http://www.opensource.org/licenses/mit-license.php.
 */

/* global describe it */

const { assert } = require('chai');
const cashaddr = require('../src/cashaddr');
const Random = require('random-js');

describe('cashaddr', () => {

  const NETWORKS = ['nexa', 'nexatest', 'nexareg'];
  
  const ADDRESS_TYPES = ['P2PKH', 'TEMPLATE'];
  
  const VALID_SIZES = [20, 24, 28, 32, 40, 48, 56, 64];

  /* Generate with kotlin code
    val v = listOf(intArrayOf(118, 160, 64,  83, 189, 160, 168, 139, 218, 81, 119, 184, 106, 21, 195, 178, 159, 85,  152, 115).map{ it.toByte()}.toByteArray(), intArrayOf(203, 72, 18, 50, 41,  156, 213, 116, 49,  81, 172, 75, 45, 99, 174, 25,  142, 123, 176, 169).map{ it.toByte()}.toByteArray(), intArrayOf(1,   31, 40,  228, 115, 201, 95, 64,  19,  215, 213, 62, 197, 251, 195, 180, 45, 248, 237, 16).map{ it.toByte()}.toByteArray())

    v.map { "'" + PayAddress(ChainSelector.NEXA, PayAddressType.P2PKH, it).toString() + "'" }.joinToString(", ")
    v.map { "'" + PayAddress(ChainSelector.NEXATESTNET, PayAddressType.P2PKH, it).toString() + "'" }.joinToString(", ")
    v.map { "'" + PayAddress(ChainSelector.REGTEST, PayAddressType.P2PKH, it).toString() + "'" }.joinToString(", ")

 val cs = ChainSelector.NEXA
 val n = listOf(SatoshiScript.ungroupedP2pkt(cs, intArrayOf(118, 160, 64,  83, 189, 160, 168, 139, 218, 81, 119, 184, 106, 21, 195, 178, 159, 85,  152, 115).map{ it.toByte()}.toByteArray()), SatoshiScript.ungroupedP2pkt(cs, intArrayOf(203, 72, 18, 50, 41,  156, 213, 116, 49,  81, 172, 75, 45, 99, 174, 25,  142, 123, 176, 169).map{ it.toByte()}.toByteArray()), SatoshiScript.ungroupedP2pkt(cs, intArrayOf(1,   31, 40,  228, 115, 201, 95, 64,  19,  215, 213, 62, 197, 251, 195, 180, 45, 248, 237, 16).map{ it.toByte()}.toByteArray()))


  // TEMPLATE_TEST_DATA bytes
  n.map { it.asSerializedByteArray().map{ it.toUByte().toString() }.joinToString(", ","[","]")}.joinToString(",")

  // proper addresses
  // Nexa
  n.map { "'" + it.address.toString() + "'" }.joinToString(", ")
  // testnet
  val t = n.map { PayAddress(ChainSelector.NEXATESTNET, it.address!!.type, it.address!!.data) }
  t.map { "'" + it.toString() + "'" }.joinToString(", ") 
  // regtest
  val r = n.map { PayAddress(ChainSelector.REGTEST, it.address!!.type, it.address!!.data) }
  r.map { "'" + it.toString() + "'" }.joinToString(", ") 
  */
    
  const TEST_HASHES = [
    new Uint8Array([118, 160, 64,  83, 189, 160, 168, 139, 218, 81, 119, 184, 106, 21, 195, 178, 159, 85,  152, 115]),
    new Uint8Array([203, 72, 18, 50, 41,  156, 213, 116, 49,  81, 172, 75, 45, 99, 174, 25,  142, 123, 176, 169]),
    new Uint8Array([1,   31, 40,  228, 115, 201, 95, 64,  19,  215, 213, 62, 197, 251, 195, 180, 45, 248, 237, 16]),
  ];

  const TEMPLATE_TEST_DATA = [
    new Uint8Array([23, 0, 81, 20, 215, 141, 90, 154, 136, 165, 86, 167, 64, 194, 211, 196, 224, 108, 150, 134, 162, 59, 13, 173]),
    new Uint8Array([23, 0, 81, 20, 14, 117, 126, 87, 128, 164, 50, 254, 79, 91, 119, 10, 243, 79, 60, 100, 248, 85, 8, 73]),
    new Uint8Array([23, 0, 81, 20, 136, 147, 216, 155, 75, 229, 39, 244, 234, 1, 140, 161, 16, 91, 165, 141, 252, 222, 164, 44]),
  ];

    
  const EXPECTED_P2PKH_OUTPUTS = [
    'nexa:qpm2qsznhks23z7629mms6s4cwef74vcwvgpsey0xy', 'nexa:qr95sy3j9xwd2ap32xkykttr4cvcu7as4yrtkg2qqa', 'nexa:qqq3728yw0y47sqn6l2na30mcw6zm78dzq0jl7vjk6'
  ];

  const EXPECTED_TEMPLATE_OUTPUTS = [
    'nexa:nqtsq5g567x44x5g54t2wsxz60zwqmyks63rkrddsfq94pd2', 'nexa:nqtsq5g5pe6hu4uq5se0un6mwu90xneuvnu92zzf850pfkqg', 'nexa:nqtsq5g53zfa3x6tu5nlf6sp3js3qka93h7dafpvjaw8nmvm'
  ];

  const EXPECTED_P2PKH_OUTPUTS_TESTNET = [
    'nexatest:qpm2qsznhks23z7629mms6s4cwef74vcwvx26kzw52', 'nexatest:qr95sy3j9xwd2ap32xkykttr4cvcu7as4ydqu8vpjn', 'nexatest:qqq3728yw0y47sqn6l2na30mcw6zm78dzqpe432ny5'
  ];

  const EXPECTED_TEMPLATE_OUTPUTS_TESTNET = [
    'nexatest:nqtsq5g567x44x5g54t2wsxz60zwqmyks63rkrddl4stwnzu', 'nexatest:nqtsq5g5pe6hu4uq5se0un6mwu90xneuvnu92zzfggl0jy07', 'nexatest:nqtsq5g53zfa3x6tu5nlf6sp3js3qka93h7dafpvap7fgfrd'
  ];

  const EXPECTED_P2PKH_OUTPUTS_REGTEST = [
    'nexareg:qpm2qsznhks23z7629mms6s4cwef74vcwv3th0z6u8', 'nexareg:qr95sy3j9xwd2ap32xkykttr4cvcu7as4y6p37v467', 'nexareg:qqq3728yw0y47sqn6l2na30mcw6zm78dzqkccg28ve'
  ];

  const EXPECTED_TEMPLATE_OUTPUTS_REGTEST = [
    'nexareg:nqtsq5g567x44x5g54t2wsxz60zwqmyks63rkrddv9hv06gc', 'nexareg:nqtsq5g5pe6hu4uq5se0un6mwu90xneuvnu92zzfmccgnd96', 'nexareg:nqtsq5g53zfa3x6tu5nlf6sp3js3qka93h7dafpvw3ewfqff'
  ];

  const random = new Random(Random.engines.mt19937().seed(42));

  function getRandomHash(size) {
    const hash = new Uint8Array(size);
    for (let i = 0; i < size; ++i) {
      hash[i] = random.integer(0, 255);
    }
    return hash;
  }

  describe('#encode()', () => {
    it('should fail on an invalid prefix', () => {
      assert.throws(() => {
        cashaddr.encode('some invalid prefix', ADDRESS_TYPES[0], new Uint8Array([]));
      }, cashaddr.ValidationError);
    });

    it('should fail on a prefix with mixed letter case', () => {
      assert.throws(() => {
        cashaddr.encode('NeXa', ADDRESS_TYPES[0], new Uint8Array([]));
      }, cashaddr.ValidationError);
    });

    it('should fail on an invalid type', () => {
      assert.throws(() => {
        cashaddr.encode(NETWORKS[0], 'some invalid type', new Uint8Array([]));
      }, cashaddr.ValidationError);
    });

    it('should encode test hashes on mainnet correctly', () => {
      for (const index in TEST_HASHES) {
        assert.equal(
          cashaddr.encode('nexa', 'P2PKH', TEST_HASHES[index]),
          EXPECTED_P2PKH_OUTPUTS[index]
        );
        assert.equal(
          cashaddr.encode('nexa', 'TEMPLATE', TEMPLATE_TEST_DATA[index]),
          EXPECTED_TEMPLATE_OUTPUTS[index]
        );
      }
    });

    it('should encode test hashes on testnet correctly', () => {
      for (const index in TEST_HASHES) {
        assert.equal(
          cashaddr.encode('nexatest', 'P2PKH', TEST_HASHES[index]),
          EXPECTED_P2PKH_OUTPUTS_TESTNET[index]
        );
        assert.equal(
          cashaddr.encode('nexatest', 'TEMPLATE', TEMPLATE_TEST_DATA[index]),
          EXPECTED_TEMPLATE_OUTPUTS_TESTNET[index]
        );
      }
    });

    it('should encode test hashes on regtest correctly', () => {
      for (const index in TEST_HASHES) {
        assert.equal(
          cashaddr.encode('nexareg', 'P2PKH', TEST_HASHES[index]),
          EXPECTED_P2PKH_OUTPUTS_REGTEST[index]
        );
        assert.equal(
          cashaddr.encode('nexareg', 'TEMPLATE', TEMPLATE_TEST_DATA[index]),
          EXPECTED_TEMPLATE_OUTPUTS_REGTEST[index]
        );
      }
    });
  });


    
  describe('#decode()', () => {
    it('should fail when the version byte is invalid', () => {
      assert.throws(() => {
        cashaddr.decode('nexa:zpm2qsznhks23z7629mms6s4cwef74vcwvrqekrq9w');
      }, cashaddr.ValidationError);
    });

    it('should fail when given an address with mixed letter case', () => {
      assert.throws(() => {
        cashaddr.decode('NEXA:NQTSQ5G567X44X5G54T2WSXZ60ZWQMYKS63RKRdDSFQ94PD2');
      }, cashaddr.ValidationError);
      assert.throws(() => {
        cashaddr.decode('Nexa:nqtsq5g567x44x5g54t2wsxz60zwqmyks63rkrddsfq94pd2');
      }, cashaddr.ValidationError);
      assert.throws(() => {
        cashaddr.decode('Nexa:nQTsq5g567x44x5g54t2wsxz60zwqmyks63rkrddsfq94pd2');
      }, cashaddr.ValidationError);
    });

    it('should decode a valid address regardless of letter case', () => {
      assert.deepEqual(
        cashaddr.decode('nexa:nqtsq5g567x44x5g54t2wsxz60zwqmyks63rkrddsfq94pd2').hash,
        cashaddr.decode('NEXA:NQTSQ5G567X44X5G54T2WSXZ60ZWQMYKS63RKRDDSFQ94PD2').hash
      );
    });
    
    it('should fail when decoding for a different network', () => {
      for (const network of NETWORKS) {
        for (const anotherNetwork of NETWORKS) {
          if (network !== anotherNetwork) {
            const hash = getRandomHash(20);
            assert.throws(() => {
              const address = cashaddr.encode(network, ADDRESS_TYPES[0], hash);
              const invalidAddress = [anotherNetwork, address.split(':')[1]].join(':');
              cashaddr.decode(invalidAddress);
            }, cashaddr.ValidationError);
          }
        } 
      }
    });
  });
  
  describe('#encode() #decode()', () => {
    it('should encode and decode all sizes correctly', () => {
      for (const size of VALID_SIZES) {
        const hash = getRandomHash(size);
        const address = cashaddr.encode(NETWORKS[0], ADDRESS_TYPES[0], hash);
        const { prefix, type, hash: actualHash } = cashaddr.decode(address);
        assert.equal(prefix, NETWORKS[0]);
        assert.equal(type, ADDRESS_TYPES[0]);
        assert.deepEqual(actualHash, hash);
      }
    });
  
    it('should encode and decode all types and networks', () => {
      for (const type of ADDRESS_TYPES) {
        for (const network of NETWORKS) {
          const hash = getRandomHash(20);
          const address = cashaddr.encode(network, type, hash);
          const { prefix, type: actualType, hash: actualHash } = cashaddr.decode(address);
          assert.equal(prefix, network);
          assert.equal(actualType, type);
          assert.deepEqual(actualHash, hash);
        }
      }
    });
  
    it('should encode and decode many random hashes', () => {
      const NUM_TESTS = 1000;
      for (let i = 0; i < NUM_TESTS; ++i) {
        for (const type of ADDRESS_TYPES) {
          const hash = getRandomHash(20);
          const address = cashaddr.encode(NETWORKS[0], type, hash);
          const { prefix, type: actualType, hash: actualHash } = cashaddr.decode(address);
          assert.equal(prefix, NETWORKS[0]);
          assert.equal(actualType, type);
          assert.deepEqual(actualHash, hash);
        }
      }
    });
  });
});
