# CashAddr.js: The new Bitcoin Cash address format for Node.js and web browsers.

[![Build Status](https://travis-ci.org/nexa/nexaaddrjs.svg?branch=nexa)](https://travis-ci.org/nexa/nexaaddrjs) [![Coverage Status](https://coveralls.io/repos/gitlab/nexa/nexaaddrjs/badge.svg?branch=nexa)](https://coveralls.io/gitlab/nexa/nexaaddrjs?branch=nexa)

<!-- [![NPM](https://nodei.co/npm/cashaddrjs.png?downloads=true)](https://nodei.co/npm/cashaddrjs/) -->

JavaScript implementation for the Nexa cryptocurrency address format.

Compliant with the Nexa address [specification](http://spec.nexa.org/protocol/blockchain/encoding/cashaddr).

## Installation

### Using NPM

```bsh
$ npm install --save nexaaddrjs
```

### Using Bower

```bsh
$ bower install --save nexaaddrjs
```

### Manually

<!-- TBD: You may also download the distribution file manually and place it within your third-party scripts directory: [dist/cashaddrjs-0.4.4.min.js](https://unpkg.com/cashaddrjs@0.4.4/dist/cashaddrjs-0.4.4.min.js). -->

## Usage

### In Node.js

```javascript
const nexaaddr = require('nexaaddrjs');
const address = 'nexatest:qzmzm493h5j67z2zk2lsag4qeye02x5pxyrlswqv76';
const { prefix, type, hash } = nexaaddr.decode(address);
console.log(prefix); // 'nexatest'
console.log(type); // 'P2PKH'
console.log(hash); // Uint8Array [... ]
console.log(nexaaddr.encode(prefix, type, hash)); // 'nexatest:qzmzm493h5j67z2zk2lsag4qeye02x5pxyrlswqv76'
```

### Example with bitcore-lib

This example decodes a Nexa cashaddr P2PKH address and loads it into a bitcore-lib Address.  Even though bitcore-lib is for Bitcoin, Nexa P2PKH is equivalent to Bitcoin/Bitcoin Cash P2PKH so some bitcore-lib operations apply.
```javascript
var bitcore = require("bitcore-lib");
var nexaaddr = require("nexaaddrjs");

const fixAddressFormat = address => {
    let addrBytes = nexaaddr.decode(address);
    let buf = addrBytes.hash.buffer.slice(1,21);  // ignore the type byte
    let buf2 = Buffer.from(buf);
    console.log(buf);
    console.log(buf2);
    return bitcore.Address.fromPublicKeyHash(buf2, "livenet")
}
```

### Browser

#### Script Tag

You may include a script tag in your HTML and the `cashaddr` module will be defined globally on subsequent scripts.

```html
<html>
  <head>
    ...
    <script src="nexaaddrjs-1.0.0.min.js"></script>
  </head>
  ...
</html>
```

## Development

Clone the project

```bsh
$ npm install
```

### Test

```bsh
$ npm run test
```
### Install Locally in a Different Project
```bsh
 npm pack
 npm install /path_to_this_project/nexaaddrjs-1.0.0.tgz 
```

### Package

```bsh
$ npm pack
```

## Documentation

### Generate and Browse Locally

```bsh
$ npm run docs
```

### Online

Browse automatically generated jsdocs [online](https://todo).
